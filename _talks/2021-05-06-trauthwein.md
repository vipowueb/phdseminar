---
layout:     talk
title:      "Stein's Method"
speaker:    Tara TRAUTHWEIN
categories: talks
---
Convergence in distribution is a classical topic in probability theory and there is a plethora of methods and tools to show the convergence of a sequence of random variables towards a limiting distribution. However, it is often harder to find the rate of convergence. In 1972, Charles Stein introduced an idea on how to approach this problem in the Gaussian case. His method is now known as Stein's Method and has found countless applications and generalizations to other distributions.

In this talk I am going to give an introduction to the ideas behind Stein's Method in the Gaussian and Poisson case and I am going to show how to generalize this idea to distributions with absolutely continuous densities.
